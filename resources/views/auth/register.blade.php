@extends('layouts.app')
@section('after_styles')

    <style>
        .btn-orange, .btn-orange-border {
    border-color: #f96f1e!important;
    font-size: 16px;
    font-family: Archivo,sans-serif;
    font-weight: 400;
    padding: 10px 0;
    color:#fff;
    background: #f96f1e!important;
}

        /*Check box*/



@keyframes effect{
    0%{transform: scale(0);}
    25%{transform: scale(1.3);}
    75%{transform: scale(1.4);}
    100%{transform: scale(1);}
}
 #myModal .modal-dialog {
    -webkit-transform: translate(0,-50%);
    -o-transform: translate(0,-50%);
    transform: translate(0,-50%);
    margin: 0 auto;
  width:836px;
  top:60%;
  height:550px;

}
#myModal .modal-body {
    max-height: 2000px!important;
    overflow-y: scroll;
    max-height: 550px !important;
    z-index:88;
    display:block;
    padding-top: 50px;
    padding-bottom:80px;
}
.fixed-header {
    top: 0;
}
.fixed-footer{
        bottom: 0;
    }
    .modal-body p{
        line-height: 25px; /* Create scrollbar to test positioning */
    }
    .fixed-header{background-color: #ff711e !important; box-shadow: 0px 3px 6px #00000029!important;}
.fixed-header, .fixed-footer {
    width: 100%;
    position: fixed;
    background: #fff;
    padding: 10px 0;
    color: #fff;
    z-index: 888;
}
        #rc-imageselect, .g-recaptcha {transform:scale(0.77);-webkit-transform:scale(0.77);transform-origin:0 0;-webkit-transform-origin:0 0;}
        @media screen and (max-height: 575px){
            #rc-imageselect, .g-recaptcha {transform:scale(0.77);-webkit-transform:scale(0.77);transform-origin:0 0;-webkit-transform-origin:0 0;
            }}
            .ui-menu{
                max-height:300px;
                overflow-x: hidden;
                overflow-y: auto;
            }
            .select2-container{
                display: block!important;
                width:100%!important;
            }
            .logo-block a img{width:25%;padding:35px 0;}
        }

        @media screen and (max-width: 768px) {
#myModal .modal-dialog{width:100% !important;top: 50% !important;}
}
@media only screen and (max-width: 520px) and (min-width: 320px){
#myModal .modal-dialog {
    top: 45% !important;
    height: 600px !important;
    width:100% !important;
}
}
    </style>
@endsection
@section('after_styles')

    <style>
        .btn-orange, .btn-orange-border {
    border-color: #f96f1e!important;
    font-size: 16px;
    font-family: Archivo,sans-serif;
    font-weight: 400;
    padding: 10px 0;
    color:#fff;
    background: #f96f1e!important;
}

        /*Check box*/



@keyframes effect{
    0%{transform: scale(0);}
    25%{transform: scale(1.3);}
    75%{transform: scale(1.4);}
    100%{transform: scale(1);}
}
 #myModal .modal-dialog {
    -webkit-transform: translate(0,-50%);
    -o-transform: translate(0,-50%);
    transform: translate(0,-50%);
    margin: 0 auto;
  width:836px;
  top:60%;
  height:550px;

}
#myModal .modal-body {
    max-height: 2000px!important;
    overflow-y: scroll;
    max-height: 550px !important;
    z-index:88;
    display:block;
    padding-top: 50px;
    padding-bottom:80px;
}
.fixed-header {
    top: 0;
}
.fixed-footer{
        bottom: 0;
    }
    .modal-body p{
        line-height: 25px; /* Create scrollbar to test positioning */
    }
    .fixed-header{background-color: #ff711e !important; box-shadow: 0px 3px 6px #00000029!important;}
.fixed-header, .fixed-footer {
    width: 100%;
    position: fixed;
    background: #fff;
    padding: 10px 0;
    color: #fff;
    z-index: 888;
}
        #rc-imageselect, .g-recaptcha {transform:scale(0.77);-webkit-transform:scale(0.77);transform-origin:0 0;-webkit-transform-origin:0 0;}
        @media screen and (max-height: 575px){
            #rc-imageselect, .g-recaptcha {transform:scale(0.77);-webkit-transform:scale(0.77);transform-origin:0 0;-webkit-transform-origin:0 0;
            }}
            .ui-menu{
                max-height:300px;
                overflow-x: hidden;
                overflow-y: auto;
            }
            .select2-container{
                display: block!important;
                width:100%!important;
            }
            .logo-block a img{width:25%;padding:35px 0;}
        }

        @media screen and (max-width: 768px) {
#myModal .modal-dialog{width:100% !important;top: 50% !important;}
}
@media only screen and (max-width: 520px) and (min-width: 320px){
#myModal .modal-dialog {
    top: 45% !important;
    height: 600px !important;
    width:100% !important;
}
}
    </style>
@endsection
@section('content')
 <section class="sign-up-section">
    @if (\Session::has('error'))
    <div class="alert alert-warning">
        <ul>
            <li>{!! \Session::get('error') !!}</li>
        </ul>
    </div>
@endif
        <div class="container">

            <form id="signup_form" role="form" method="POST" action="{{ url('/register') }}" data-parsley-validate="" autocomplete="off">
                {{ csrf_field() }}
                    
                <div class="signup-form">
                    <div class="text-center logo-block">
                        <a href="{{ url('/') }}">     
                        </a>
                        
                    </div>

                    <h3>Register to Globus Soft
                    </h3>

                    <ul class="list-unstyled">
                        <li class="row">
                            <div class="col-md-6">
                     
</li>
                        <li class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label >First Name <span style="color: #ff0000; font-size: 12px">*</span></label>
                                    <input id="name" type="text" class="form-control" name="first_name" autofocus required placeholder="First Name" data-parsley-pattern="[ a-zA-Z]*">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Last Name <span style="color: #ff0000; font-size: 12px">*</span></label>
                                    <input id="name" type="text" class="form-control" name="last_name" required placeholder="Last Name" data-parsley-pattern="[ a-zA-Z]*">
                                </div>
                            </div>
                        </li>

                        <li class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email Address <span style="color: #ff0000; font-size: 12px">*</span></label>
                                    <input id="UserEmail" type="email" class="form-control" name="email" value="{{ $emailId }}"  required data-parsley-type="Email" data-parsley-pattern='/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/'>

                                    <span class="paragraph" id="email-data">Registered email for 
                                   Globus Soft 
                                Used for re-login</span>
                                </div>
                            </div>
                          
                        </li>

                        <li class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Create Account Password <span style="color: #ff0000; font-size: 12px">*</span></label>
                                    <input id="password" type="password" class="form-control password-checking" name="password" required minLength="9" data-parsley-pattern="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).*$" data-parsley-pattern-message="Your password must contain at least (1) lowercase and (1) uppercase letter.(1) digit,(1) special character">
                                    @if ($errors->has('password')) <small style="color:red;" class="help-text">{{ $errors->first('password') }}</small> @endif

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Confirm Password <span style="color: #ff0000; font-size: 12px">*</span></label>
                                    <input id="confirm" type="password" class="form-control password-checking" name="password_confirmation" required>
                                    <span class="password-error" style="position: absolute;"> </span>
                                </div>
                            </div>
                              
                        </li>
                       

                        <li>
                    
                            <div class="signup-btn">
                                <div class="form-group">
                                    <input type="submit" class="sub" value="REGISTER NOW" />
                                    <div class="pull-right hide" style="margin-top:-47px">
                                       
                                    </div>
                                </div>
                            </div>

                        </li>
                         <li>
   <p class="text-center" id="demo"> </p>
    <li>

                        <p class="text-center">Already
                            
                                  Registered
                                   
                         user?

                            
                            <a href="{{ url('/login') }}">
                                Login
                            </a>
                            
                            here</p>
                       
                            
                           
                    </ul>
                </div>
            </form>

        </div>
    </section>
@endsection
@section('after_scripts')
       <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
       <script>

    window.Laravel = {
        csrfToken: '{{csrf_token()}}'
    }
</script>
<script>
</script>
        <script type="text/javascript">
        var oldText='';
        $('document').ready(function(){

            $('#UserEmail').blur(function(){
                oldText=$('#email-data').html();
                var email=$(this).val();
                if(email!=''){
                    $.ajax({
                        url: '../api/email/' + email,
                        type: 'GET',
                        timeout: 30000,
                        success: function(response){
                           
                            if(response.statusCode== 200){
                                $('#email-data').html('Email already exists').css('color','red');
                            }else{
                                $('#email-data').html('<i class="fa fa-check" style="color:#449D44;"></i> You are Unique').css('color','green');
                            }
                        },error: function(){

                        }

                    });
                }
            });
            $('#UserEmail').focus(function() {
                if(oldText != ''){
                    $('#email-data').html(oldText).css('color','#656a70');
                }
            });

            $('.password-checking').blur(function(){
                var password=$('#password').val();
                var confirmPassword=$('#confirm').val();
                if(password != ''){
                    if(password != confirmPassword){
                        if(confirmPassword !=''){
                            $('.password-error').html('Password is not Matching').css('color','red');
                        }
                    }else{
                        $('.password-error').html('Password Matches').css('color','green');
                    }
                }

            });
            $("#signup_form").submit(function(){

                var password=$('#password').val();
                var confirmPassword=$('#confirm').val();
                if(password != ''){
                    if(password != confirmPassword){
                        if(confirmPassword !=''){
                            $('.password-error').html('Password is not Matching').css('color','red');
                            return false;
                        }
                    }else{
                        $('.password-error').html('Password Matches').css('color','green');
                        return true;
                    }
                }

            });

            $('.password-checking').focus(function(){
                $('.password-error').html('');
            });     
            
        });
    </script>

@endsection
