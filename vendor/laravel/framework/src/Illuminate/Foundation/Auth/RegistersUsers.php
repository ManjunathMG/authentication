<?php

namespace Illuminate\Foundation\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use Session;
use App\Admin;
use Illuminate\Support\Facades\Validator;
trait RegistersUsers
{
    use RedirectsUsers;

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {  $emailId = '';
       return view('auth.register', [
            'emailId' => $emailId,
             ]);
    }
    public function showAdminRegistrationForm()
    {  $emailId = '';
       return view('auth.admin_register', [
            'emailId' => $emailId,
             ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $emailId = '';
if(isset($request['email']))
{
    


    $emailId = $request['email'];
    $registered_user=User::where('email',$request['email'])->get();
    if(count($registered_user)>0)
    {
        return redirect()->back()->with('error', 'email already in use, please use other one');   
    }
    else{

                $user = new User;
                $user->name = $request['first_name'] . ' ' . $request['last_name']; 
                $user->email = strtolower($request['email']);
                $user->password=bcrypt($request['password']);
                $user->save();
                return \Redirect::to('login');
        }
    }
return view('auth.register', [
            'emailId' => $emailId,
            'title' => 'Register',
            // 'college' => $college,
            // 'company' => $company,
            // 'department' => $department,
            // 'degree' => $degree,
            // 'prof_college' => $prof_college,
        ]);
    }
      public function admin_register(Request $request)
    {
        $emailId = '';
if(isset($request['email']))
{
    $emailId = $request['email'];
    $registered_user=Admin::where('email',$request['email'])->get();
    if(count($registered_user)>0)
    {
        return redirect()->back()->with('error', 'email already in use, please use other one');   
    }
    else{

                $user = new Admin;
                $user->name = $request['first_name'] . ' ' . $request['last_name']; 
                $user->job_title='Admin';
                $user->email = strtolower($request['email']);
                $user->password=bcrypt($request['password']);
                $user->save();
                return \Redirect::to('admin/login');
        }
    }
return view('auth.register', [
            'emailId' => $emailId,
            'title' => 'Register',
            // 'college' => $college,
            // 'company' => $company,
            // 'department' => $department,
            // 'degree' => $degree,
            // 'prof_college' => $prof_college,
        ]);
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        //
    }
}
