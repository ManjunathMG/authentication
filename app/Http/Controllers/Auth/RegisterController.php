<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Session;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
    public function get_email($email_id) {
  // dd($email);
        if (isset($email_id)) {

            $user_data = User::where('email', $email_id)->first();
            $user_data_admin = Admin::where('email', $email_id)->first();

            if ((!empty($user_data)) ){
                return response()->json(
                    [
                        'status' => 'success',
                        'message' => 'User detail',
                        'success' => 'User detail found',
                        'statusCode' => 200,
                    ]
                );
            } elseif ((!empty($user_data_admin))) {
                 return response()->json(
                    [
                        'status' => 'success',
                        'message' => 'User detail',
                        'success' => 'User detail found',
                        'statusCode' => 200,
                    ]
                );
            }


            else {
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'User not found',
                        'error' => 'User not found',
                        'statusCode' => 500,
                    ]
                );
            }
        } else {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Email Id required',
                    'success' => 'Email Id required',
                    'statusCode' => 404,
                ]
            );
        }
    }
}
